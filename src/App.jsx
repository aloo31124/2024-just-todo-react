import { useState } from 'react'
import './App.css'
import Header from './components/header/Header'
import List from './components/list/List'

function App() {

  const [todoList, _setTodoList] = useState(
    [
      {id:"a0001", name:"早上放鬆冥想2分鐘 :) ", isDone: true},
      {id:"a0002", name:"下班看一本放鬆有趣的書兩頁", isDone: true},
      {id:"a0003", name:"下班機車加油", isDone: false},
    ]
  );

  /* 
   * 新增 [待辦事項]
   */
  const addTodo = (newTodo) => {
    const newTodoList = [...todoList, newTodo];
    _setTodoList(newTodoList);
  }

  /* 
   * 刪除 [待辦事項]
   */
  const deleteTodo = (id) => {
    const newTodoList = todoList.filter(todo =>  todo.id !== id);
    _setTodoList(newTodoList);    
  }

  return (
    <>
      <Header addTodo={addTodo}/>
      <List todoList={todoList} deleteTodo={deleteTodo}/>
    </>
  )
}

export default App
