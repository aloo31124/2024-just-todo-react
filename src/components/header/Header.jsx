import { useRef } from "react";
import { nanoid } from "nanoid";


function Header(props) {

    // 建立 新增待辦事項 ipnut ref容器
    const addTodoInput = useRef();

    /* 
     * 新增 [待辦事項]
     */
    const addTodo = () => {
        props.addTodo({id:nanoid(), name:addTodoInput.current.value, isDone:false});
        addTodoInput.current.value = "";
    }

    return (
        <div>
            <input ref={addTodoInput} type="text" placeholder="輸入待作項目"/>
            <button onClick={addTodo}>新增</button>
        </div>
    );    
}

export default Header;
