
function TodoItem(props) {

    /* 
     * 刪除 [待辦事項]
     */
    const deleteTodo = () => {
        props.deleteTodo(props.id);
    }    
    
    return (
        <div>
            <input type="checkbox" defaultChecked={props.isDone}/>
            待作項目: {props.name}
            <button onClick={deleteTodo}>刪除</button>
        </div>        
    );
}

export default TodoItem;
