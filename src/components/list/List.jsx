import TodoItem from "../item/TodoItem";

function List(props) {

    return(
        <div>
            {
                props.todoList.map(todo => {
                    return <TodoItem key={todo.id} {...todo} deleteTodo={props.deleteTodo} />
                })
            }
        </div>
    );

}

export default List;
